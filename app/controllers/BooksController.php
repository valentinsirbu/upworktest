<?php

class BooksController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$books = Book::all();
		return View::make('books', array('books' => $books));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$authors = Author::lists('name', 'id');
		return View::make('create', array('authors' => $authors));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$book = new Book;
		$book->title = Input::get('title');
		$book->description = Input::get('description');
		$book->author_id = Input::get('author_id');
		$book->save();
		$author = Author::find(Input::get('author_id'));


		foreach($author->subscribers as $subscriber){
			Mail::send('emails.subscribe', array('key' => 'value'), function($message) use ($subscriber)
			{
				$message->to("$subscriber->email", 'John Smith')->subject('Books Update!');
			});
		}
		return Redirect::to('books');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$book = Book::find($id);
		return View::make('show', array('book' => $book));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$authors = Author::lists('name', 'id');
		return View::make('create', array('authors' => $authors));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		Book::find($id);
		$book = new Book;
		$book->title = Input::get('title');
		$book->description = Input::get('description');
		$book->author_id = Input::get('author_id');
		$book->update();
		return Redirect::to('books');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Book::find($id)->delete();
		return Redirect::to('books');
	}


}
