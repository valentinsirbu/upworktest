<?php


class AccountController extends BaseController {
	public function login() {
		// Getting all post data
		$data = Input::all();
		// Applying validation rules.
		$rules = array(
			'login' => 'required',
			'password' => 'required',
		);
		$validator = Validator::make($data, $rules);
		if ($validator->fails()){
			// If validation fails redirect back to login.
			return Redirect::to('/login')->withInput(Input::except('password'))->withErrors($validator);
		}
		else {
			$userdata = array(
				'login' => Input::get('login'),
				'password' => Input::get('password')
			);
			// doing login.
			if (Auth::validate($userdata)) {
				if (Auth::attempt($userdata)) {
					return Redirect::intended('/books');
				}
			}
			else {
				// if any error send back with message.
				Session::flash('error', 'Something went wrong');
				return Redirect::to('login');
			}
		}
	}
}


