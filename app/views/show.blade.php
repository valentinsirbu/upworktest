

@extends('layout')
@section('content')
    <table class="table">
        <h2>{{$book->title}}</h2>
        <p>{{$book->description}}</p>
        <a href="authors/{{$book->author->id}}">{{$book->author->name}}</a>
        <br>
        {{ Form::open(array('url' => 'books/' . $book->id, 'class' => 'pull-right')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Delete this Book', array('class' => 'btn btn-danger')) }}
        {{ Form::close() }}
        <a href="books/{{$book->id}}/edit" class="btn btn-primary">Edit</a>

    </table>
@stop