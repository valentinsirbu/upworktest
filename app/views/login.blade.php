

@extends('layout')
@section('content')
    {{ Form::open(array('url' => 'login')) }}
    <h1>Login</h1>
    @if(Session::has('error'))
        <div class="alert-box success">
            <h2>{{ Session::get('error') }}</h2>
        </div>
    @endif

    <div class="col-md-6">
        <div class="controls">
            {{ Form::text('login','',array('id'=>'','class'=>'form-control span6','placeholder' => 'Please Enter your Username')) }}
            <p class="errors">{{$errors->first('login')}}</p>
        </div>
        <div class="controls">
            {{ Form::password('password',array('class'=>'form-control span6', 'placeholder' => 'Please Enter your Password')) }}
            <p class="errors">{{$errors->first('password')}}</p>
        </div>
    </div>

    <div class="col-md-2">
        <p>{{ Form::submit('Login', array('class'=>'send-btn btn btn-primary')) }}</p>
    </div>

    {{ Form::close() }}
@stop