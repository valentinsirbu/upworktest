
{{--<!doctype html>--}}
{{--<html>--}}
{{--<head>--}}
    {{--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="container">--}}
    {{--{{ Form::open(array('url' => 'books')) }}--}}
    {{--<h1>Create</h1>--}}
    {{--@if(Session::has('error'))--}}
        {{--<div class="alert-box success">--}}
            {{--<h2>{{ Session::get('error') }}</h2>--}}
        {{--</div>--}}
    {{--@endif--}}

    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="controls">--}}
                {{--<label for="title">Title</label>--}}
                {{--{{ Form::text('title','',array('class'=>'form-control span6')) }}--}}
                {{--<p class="errors">{{$errors->first('login')}}</p>--}}
            {{--</div>--}}
            {{--<div class="controls">--}}
                {{--<label for="title">Title</label>--}}
                {{--{{ Form::text('description','',array('class'=>'form-control span6') )}}--}}
                {{--<p class="errors">{{$errors->first('description')}}</p>--}}
            {{--</div>--}}
            {{--<div class="controls">--}}
                {{--<label for="title">Author</label>--}}
                {{--{{ Form::select('author_id', $authors, array('class'=>'form-control select span6')) }}--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row">--}}

        {{--<div class="col-md-2">--}}
            {{--<p>{{ Form::submit('Create', array('class'=>'send-btn btn btn-primary')) }}</p>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--{{ Form::close() }}--}}
{{--</div>--}}
{{--</body>--}}
{{--</html>--}}

@extends('layout')
@section('content')
    {{ Form::open(array('url' => 'books')) }}
    <h1>Create</h1>
    @if(Session::has('error'))
        <div class="alert-box success">
            <h2>{{ Session::get('error') }}</h2>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <div class="controls">
                <label for="title">Title</label>
                {{ Form::text('title','',array('class'=>'form-control span6')) }}
                <p class="errors">{{$errors->first('login')}}</p>
            </div>
            <div class="controls">
                <label for="title">Title</label>
                {{ Form::text('description','',array('class'=>'form-control span6') )}}
                <p class="errors">{{$errors->first('description')}}</p>
            </div>
            <div class="controls">
                <label for="title">Author</label>
                {{ Form::select('author_id', $authors, array('class'=>'form-control select span6')) }}

            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-2">
            <p>{{ Form::submit('Create', array('class'=>'send-btn btn btn-primary')) }}</p>
        </div>
    </div>

    {{ Form::close() }}
@stop