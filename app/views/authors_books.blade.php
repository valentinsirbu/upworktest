
@extends('layout')
@section('content')
    <h1>{{$author->name}}</h1>
    <table class="table">
        <a href="/subscribe/{{$author->id}}" class="btn btn-success">Subscribe</a>
        @foreach($books as $book)
            <tr><td><a href="books/{{$book->id}}">{{$book->title}}</a></td>
                <td>{{$book->description}}</td>
                <td>{{$book->author->name}}</td>
                <td>
                    {{ Form::open(array('url' => 'books/' . $book->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Book', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                    <a href="/books/{{$book->id}}/edit" class="btn btn-primary">Edit</a>
                </td></tr>
        @endforeach
    </table>

@stop