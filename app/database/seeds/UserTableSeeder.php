
<?php

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->truncate();

        User::create(array(
            'login'     => 'admin',
            'password' => Hash::make('admin'),
        ));
    }
}