
<?php

class AuthorTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('authors')->truncate();

        Author::create(array(
            'name'     => 'John Doe',
        ));

        Author::create(array(
            'name'     => 'Jane Doe',
        ));

        Author::create(array(
            'name'     => 'Will Doe',
        ));
    }
}