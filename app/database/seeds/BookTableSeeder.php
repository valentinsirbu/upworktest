
<?php

class BookTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('books')->truncate();

        Book::create(array(
            'title'     => 'Book 1',
            'description' => 'Description 1',
            'author_id' => 1
        ));
        Book::create(array(
            'title'     => 'Book 2',
            'description' => 'Description 2',
            'author_id' => 2
        ));
        Book::create(array(
            'title'     => 'Book 3',
            'description' => 'Description 3',
            'author_id' => 3
        ));
    }
}