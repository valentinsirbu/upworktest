<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


	// GET route
	Route::get('login', function() {
		return View::make('login');
	});

Route::post('login', 'AccountController@login');
//Route::resource('books', 'BooksController');
//Route::resource('authors', 'AuthorsController');
//Route::get('subscribe/{id}', 'AuthorsController@subscribe');

Route::group(array('before' => 'auth'), function()
{

    Route::resource('books', 'BooksController');
    Route::resource('authors', 'AuthorsController');
    Route::get('subscribe/{id}', 'AuthorsController@subscribe');
});