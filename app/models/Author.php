<?php



class Author extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'authors';

    public function books()
    {
        return $this->hasMany('Book');
    }

    public function subscribers()
    {
        return $this->belongsToMany('User', 'subscribers');
    }
}