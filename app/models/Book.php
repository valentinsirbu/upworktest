<?php



class Book extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'books';

    protected $fillable = array('title', 'description', 'author_id');

    public function author()
    {
        return $this->belongsTo('Author');
    }
}